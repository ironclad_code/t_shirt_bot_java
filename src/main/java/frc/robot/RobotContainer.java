// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.Commands.Auto.Autos;

public class RobotContainer {

  private final CommandXboxController driverController = new CommandXboxController(0);
  private final CommandXboxController opperatorController = new CommandXboxController(1);

  private Sensors sensors;

  private final Autos autos;

  public RobotContainer(Sensors sensors) {
    this.sensors = sensors;

    autos = new Autos();

    configureDefaultCommands();
    configureDriveBindings();
    configureOpbindings();
  }

  private void configureDefaultCommands() {

  }

  private void configureDriveBindings() {

  }

  private void configureOpbindings() {

  }

  public Command getAutonomousCommand() {
    return autos.getChooser().getSelected();
  }
}
