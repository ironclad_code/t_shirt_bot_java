package frc.robot.Commands.Auto;

import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj2.command.Command;

public class Autos {
    private SendableChooser<Command> chooser = new SendableChooser<Command>();

    public Autos() {
        POST POSTcommand = new POST();

        chooser.setDefaultOption("POST", POSTcommand);
    }

    public SendableChooser<Command> getChooser() {
        return chooser;
    }
}
