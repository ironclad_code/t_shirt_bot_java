package frc.robot.Commands.Auto;

import edu.wpi.first.wpilibj2.command.PrintCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;

public class POST extends SequentialCommandGroup {
    //TODO POST in order to calibrate systems and test functions
    public POST() {
        addCommands(
            new PrintCommand(getName())
        );
    }
}
