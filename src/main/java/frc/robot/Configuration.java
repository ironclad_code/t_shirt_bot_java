package frc.robot;

import java.io.File;
import java.util.Map;

import com.google.gson.Gson;

public class Configuration {
    private static Map<String, String> env = System.getenv();
    private static File f = new File(env.get("HOME") + "/Config.json");
    private Gson gson = new Gson();

    private int x = 0;
    private int y = 0;
    private int z = 0;

    public void Save(Configuration c) {
        //TODO implement save
        String json = gson.toJson(c);
    }

    public void load() {
        //TODO implement load
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }
}
